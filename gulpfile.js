'use strict';

const gulp          = require('gulp');
const babel         = require('gulp-babel');
const sourcemaps    = require('gulp-sourcemaps');
const concat        = require('gulp-concat');
const sass          = require('gulp-sass');
const cleanCSS      = require('gulp-clean-css');
const minifyJS      = require('gulp-minify');
const remove        = require('gulp-clean');
const csso          = require('gulp-csso');
const rename        = require('gulp-rename');
const runSequence   = require('run-sequence');
const px2rem        = require('gulp-px2rem');
const autoprefixer  = require('gulp-autoprefixer');
const browserSync   = require('browser-sync').create();
const wait          = require('gulp-wait');

const PATHS = {
  dev: ['_dev/'],
  prod: ['_prod/']
};

//------------------------------ JS Concat Tasks

gulp.task('jsBuild', () =>
  gulp.src([
    'js/app.js',
    'js/game.js'
  ])
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(concat('assets.js'))
  .pipe(sourcemaps.write("."))
  .pipe(gulp.dest(PATHS.dev + 'js/'))
);

//------------------------------

//------------------------------ Minify JS

gulp.task('minify', () =>
  gulp.src(PATHS.dev + 'js/*.js')
  .pipe(minifyJS({
    noSource: true,
    ext: {
      min:'.js'
    }
  }))
  .pipe(gulp.dest(PATHS.prod + 'js/'))
);

//------------------------------

//------------------------------ HTML Copy

gulp.task('copyDev', () => {
  gulp.src('index.html').pipe(gulp.dest('_dev'));
  gulp.src('favicon.ico').pipe(gulp.dest('_dev'));
  gulp.src('js/levels.json').pipe(gulp.dest('_dev'));
});

gulp.task('copyProd', () => {
  gulp.src('index.html').pipe(gulp.dest('_prod'));
  gulp.src('favicon.ico').pipe(gulp.dest('_prod'));
  gulp.src('js/levels.json').pipe(gulp.dest('_prod'));
});

//------------------------------

//------------------------------ SASS Compile Tasks

gulp.task('sassDev', () =>
  gulp.src('scss/app.scss')
  .pipe(wait(500))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'expanded',
    includePaths: require('node-bourbon').includePaths
  }))
  .on('error', sass.logError)
  .pipe(px2rem({
    replace: true,
    rootValue: 10
  }))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(sourcemaps.write())
  .pipe(rename('assets.css'))
  .pipe(gulp.dest(PATHS.dev + 'css/'))
  .pipe(browserSync.stream())
);

gulp.task('sassProd', () =>
  gulp.src('scss/app.scss')
  .pipe(sass({
    outputStyle: 'compressed',
    includePaths: require('node-bourbon').includePaths
  }))
  .on('error', sass.logError)
  .pipe(px2rem({
    replace: true,
    rootValue: 10
  }))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(rename('assets.css'))
  .pipe(cleanCSS({
    keepSpecialComments: 0
  }))
  .pipe(csso({
    report: 'gzip'
  }))
  .pipe(gulp.dest(PATHS.prod + 'css/'))
);

//------------------------------

//------------------------------ Remove Folders

gulp.task('clean', () => gulp.src(['_dev', '_prod'], {read: false}).pipe(remove()));

//------------------------------

//------------------------------ Watch

gulp.task('watch', () => {
  gulp.watch('index.html', ['copyDev']);
  gulp.watch('scss/**/*.scss', ['sassDev']);
  gulp.watch('js/**/*.js', ['jsBuild']);
});

//------------------------------

//------------------------------ BrowserSync

gulp.task('brSync', () => {
  browserSync.init({
    server: {
      watchTask: true,
      baseDir: "_dev/"
    },
    port: 5555
  });

  gulp.watch('_dev/*.html').on('change', browserSync.reload);
  gulp.watch('_dev/**/*.js').on('change', browserSync.reload);
});

//------------------------------

gulp.task('default', ['brSync', 'watch']);

gulp.task('dev', () => runSequence(
  'clean', 
  'sassDev',
  'jsBuild',
  'copyDev'
));

gulp.task('prod', () => runSequence(
  'clean',
  'sassProd',
  'jsBuild',
  'minify',
  'copyProd'
));