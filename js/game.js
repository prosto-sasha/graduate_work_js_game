'use strict';

// --------------------------------------------------------------------- [Class Vector] ---------------------------------------------------

class Vector {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  plus(vector) {
    if (!(vector instanceof Vector)) {
      throw new Error('Можно прибавлять к вектору только вектор типа Vector');
    }

    const plusX = this.x + vector.x;
    const plusY = this.y + vector.y;

    return new Vector(plusX, plusY);
  }

  times(multiple) {
    const timesX = this.x * multiple;
    const timesY = this.y * multiple;

    return new Vector(timesX, timesY);
  }
}

// --------------------------------------------------------------------- [Class Actor] ---------------------------------------------------

class Actor {
  constructor(pos = new Vector(), size = new Vector(1,1), speed = new Vector()) {
    if (!(pos instanceof Vector) || !(size instanceof Vector) || !(speed instanceof Vector)) {
      throw new Error('В параметры можно передать только объект типа Vector');
    }

    this.pos = pos;
    this.size = size;
    this.speed = speed;
  }

  act () {}

  get left() {
    return this.pos.x;
  }

  get top() {
    return this.pos.y;
  }

  get right() {
    return this.left + this.size.x;
  }

  get bottom() {
    return this.top + this.size.y;
  }

  get type() {
    return 'actor';
  }

  isIntersect(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('Нужно добавить параметр или в параметре объект не типа Vector');
    }
    
    return this !== actor && this.right > actor.left && this.left < actor.right && this.bottom > actor.top && this.top < actor.bottom;
  }
}

// --------------------------------------------------------------------- [Class Level] ---------------------------------------------------

class Level {
  constructor(grid = [], actors = []) {
    this.grid = grid;
    this.actors = actors;
    this.player = this.actors.find((actor) => actor.type === 'player');
    this.height = grid.length;
    this.status = null;
    this.finishDelay = 1;
  }

  get width() {
    const rowsGridLength = this.grid.map(row => row.length);

    return this.height > 0 ? Math.max(...rowsGridLength) : 0;
  }

  isFinished() {
    return this.status !== null && this.finishDelay < 0;
  }

  actorAt(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('Нужно добавить параметр или в параметре объект не типа Vector');
    }

    return this.actors.find(item => item.isIntersect(actor));
  }

  obstacleAt(pos, size) {
    if (!(pos instanceof Vector) || !(size instanceof Vector)) {
      throw new Error('В параметры передан объект не типа Vector');
    }

    const currentActor = new Actor(pos, size);

    // const gridActors = this.grid.map((row, i) => {
    //   return row.map((cell, j) => new Actor(new Vector(j, i)));
    // });

    if (currentActor.left < 0 || currentActor.right > this.width || currentActor.top < 0) {
      return 'wall';
    } else if (currentActor.bottom > this.height) {
      return 'lava';
    }
    
    return this.grid.map(row => row[pos.x]).find(cell => cell !== undefined);


    // if (typeActor === 'wall' && ((Number.isFinite(currentActor.pos.x) && !(currentActor.pos.x % 1)) && (Number.isFinite(currentActor.pos.y) && !(currentActor.pos.y % 1)))) {
    //   return 'wall';
    // }
  }

  removeActor(actor) {
    if (actor !== undefined) {
      this.actors.splice(this.actors.indexOf(actor), 1);
    }
  }

  noMoreActors(typeActor) {
    return !this.actors.some(actor => actor.type === typeActor);
  }

  playerTouched(type, coin) {
    switch(type) {
      case 'lava':
      case 'fireball': 
        this.status = 'lost';
        break;
      case 'coin':
        if (coin !== undefined) {
          this.removeActor(coin);
        }

        const result = this.actors.find((el) => el.type === 'coin');

        if (result === undefined) {
          this.status = 'won';
        }
        break;
      default:
        return;
    }
  }
}

// --------------------------------------------------------------------- [Class LevelParse] ---------------------------------------------------

class LevelParser {
  constructor(set) {
    this.set = set;
  }

  actorFromSymbol(symbol) {
    if (symbol && this.set !== undefined) {
      return this.set[symbol];
    }
  }

  obstacleFromSymbol(symbol) {
    if (symbol) {
      switch (symbol) {
        case 'x': 
          return 'wall';
        case '!':
          return 'lava';
        default:
          return;
      }
    }
  }

  createGrid(list) {
    const listElemsGrid = list.map(str => str.split(''));

    return listElemsGrid.map(row => {
      return row.map(cell => this.obstacleFromSymbol(cell));
    });
  }

  createActors(strList) {
    const actionObjList = [];

    strList.forEach((itemX, i) => {
      [...itemX].forEach((itemY, j) => {
        if (this.actorFromSymbol(itemY) !== undefined) {
          actionObjList.push(new (this.actorFromSymbol(itemY))(new Vector(j, i)));
        }
      });
    });

    return actionObjList;
  }

  parse(plan) {
    return new Level(this.createGrid(plan), this.createActors(plan));
  }
}

// --------------------------------------------------------------------- [Class Fireball] ---------------------------------------------------

class Fireball extends Actor {
  constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
    super(pos, speed, speed);
    this.size = new Vector(1, 1);
  }

  get type() {
    return 'fireball';
  }

  getNextPosition(time = 1) {
    return new Vector(this.pos.x + (this.speed.x * time), this.pos.y + (this.speed.y * time));
  }

  handleObstacle() {
    const MULTIPLE = -1;
    const thisSpeedX = this.speed.x * MULTIPLE;
    const thisSpeedY = this.speed.y * MULTIPLE;
    this.speed = new Vector(thisSpeedX, thisSpeedY);
  }

  act(time, level) {
    if (level.obstacleAt(this.getNextPosition(time), this.size)) {
      this.handleObstacle();
    } else {
      this.pos = this.getNextPosition(time);
    }
  }
}

// --------------------------------------------------------------------- [Class HorizontalFireball] ---------------------------------------------------

class HorizontalFireball extends Fireball {
  constructor(pos) {
    super(pos, pos);
    this.speed = new Vector(2, 0);
    this.size = new Vector(1, 1);
  }
}

// --------------------------------------------------------------------- [Class VerticalFireball] ---------------------------------------------------

class VerticalFireball extends Fireball {
  constructor(pos) {
    super(pos, pos);
    this.speed = new Vector(0, 2);
    this.size = new Vector(1, 1);
  }
}

// --------------------------------------------------------------------- [Class FireRain] ---------------------------------------------------

class FireRain extends Fireball {
  constructor(pos) {
    super(pos, pos);
    this.currentPos = pos;
    this.speed = new Vector(0, 3);
    this.size = new Vector(1, 1);
  }

  handleObstacle() {
    this.pos = new Vector(this.currentPos.x, this.currentPos.y);
  }
}

// --------------------------------------------------------------------- [Class Coin] ---------------------------------------------------

class Coin extends Actor {
  constructor(pos) {
    super(pos, pos, pos);
    this.pos = this.pos.plus(new Vector(0.2, 0.1));
    this.size = new Vector(0.6, 0.6);
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.floor(Math.random() * (2 * Math.PI));
  }

  get type() {
    return 'coin';
  }

  updateSpring(time = 1) {
    this.spring += this.springSpeed * time;
  }

  getSpringVector() {
    const newY = Math.sin(this.spring) * this.springDist;
    return new Vector(0, newY);
  }

  getNextPosition(time = 1) {
    this.updateSpring(time);
    return this.pos.plus(this.getSpringVector());
  }

  act(time = 1) {
    this.pos = this.getNextPosition(time);
  }
}

// --------------------------------------------------------------------- [Class Player] ---------------------------------------------------

class Player extends Actor {
  constructor(pos) {
    super(pos, pos, pos);
    this.pos = this.pos.plus(new Vector(0, -0.5));
    this.size = new Vector(1.2, 1.2);
    this.speed = new Vector(0, 0);
  }

  get type() {
    return 'player';
  }
}

// --------------------------------------------------------------------- Start game ---------------------------------------------------

const actorDict = {
  'x': Actor,
  '!': Actor,
  '@': Player,
  'o': Coin,
  '=': HorizontalFireball,
  '|': VerticalFireball,
  'v': FireRain
};

const parser = new LevelParser(actorDict);

loadLevels()
  .then(schemas => runGame(JSON.parse(schemas), parser, DOMDisplay)
  .then(() => alert('Поздравляю! Вы прошли игру!')));